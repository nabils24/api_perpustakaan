// Init Path
__path = process.cwd();
// End Init Path

// Init Libary
var { body, validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');

// init function
exports.validate = (method) => {
    switch (method) {
        case 'createMember': {
            return [
                body('name', 'Name is required').notEmpty(),
                body('gender', 'Gender is required').notEmpty(),
                body('gender', 'Gender is not valid').isIn(['Male', 'Female']),
                body('contact', 'Contact is required').notEmpty(),
                body('contact', 'Contact is invalid').isDecimal(),
                body('address', 'address is required').notEmpty(),
            ];
        }
        case 'updateMember': {
            return [
                body('id', 'ID is required').notEmpty(),
                body('id', 'ID is invalid').isDecimal(),
                body('name', 'Name is required').notEmpty(),
                body('gender', 'Gender is required').notEmpty(),
                body('gender', 'Gender is not valid').isIn(['Male', 'Female']),
                body('contact', 'Contact is required').notEmpty(),
                body('contact', 'Contact is invalid').isDecimal(),
                body('address', 'address is required').notEmpty(),
            ];
        }
        case 'createAdmin': {
            return [
                body('name', 'Name is required').notEmpty(),
                body('contact', 'Contact is required').notEmpty(),
                body('contact', 'Contact is invalid').isDecimal(),
                body('address', 'address is required').notEmpty(),
                body('username', 'Username is required').notEmpty(),
                body('password', 'Password is required').notEmpty(),
            ];
        }
        case 'updateAdmin': {
            return [
                body('id', 'ID is required').notEmpty(),
                body('id', 'ID is invalid').isDecimal(),
                body('name', 'Name is required').notEmpty(),
                body('contact', 'Contact is required').notEmpty(),
                body('contact', 'Contact is invalid').isDecimal(),
                body('address', 'address is required').notEmpty(),
                body('username', 'Username is required').notEmpty(),
                body('password', 'Password is required').notEmpty(),
            ];
        }
        case 'createBorrow': {
            return [
                body('memberID', 'memberID is required').notEmpty(),
                body('memberID', 'memberID is invalid').isDecimal(),
                body('adminID', 'adminID is required').notEmpty(),
                body('adminID', 'adminID is invalid').isDecimal(),
                body('date_of_borrow', 'date_of_borrow is required').notEmpty(),
                body('date_of_borrow', 'date_of_borrow is invalid').isDate(),
                body('status', 'status is required').notEmpty(),
                body('status', 'status is invalid').isBoolean(),
                body('details_of_borrow', 'details_of_borrow is required').notEmpty(),
            ];
        }
        case 'updateBorrow': {
            return [
                body('id', 'ID is required').notEmpty(),
                body('id', 'ID is invalid').isDecimal(),
                body('memberID', 'memberID is required').notEmpty(),
                body('memberID', 'memberID is invalid').isDecimal(),
                body('adminID', 'adminID is required').notEmpty(),
                body('adminID', 'adminID is invalid').isDecimal(),
                body('date_of_borrow', 'date_of_borrow is required').notEmpty(),
                body('date_of_borrow', 'date_of_borrow is invalid').isDate(),
                body('status', 'status is required').notEmpty(),
                body('status', 'status is invalid').isBoolean(),
                body('details_of_borrow', 'details_of_borrow is required').notEmpty(),
            ];
        }
    }
};

exports.checkValidationResult = (request, response, next) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(422).json({ errors: errors.array() });
    } else {
        next();
    }
};