// Init Path
__path = process.cwd();
// End Init Path

// Init Var Express JS, Cors, Secure, Request, Axios, Dotenv, Fs
var express = require("express");
var bodyParser = require("body-parser");
var { body, validationResult } = require('express-validator');

// Init Router
var router = express.Router();

// Init Model
var { memberController, adminController, bukuController, borrowController, authController } = require("./../controller/index");

// Init Middleware
var checker = require("./../middleware/cek");

// set header api
router.use(function (req, res, next) {
    res.setHeader("X-Powered-By", "Nabils24-Api");
    res.setHeader("X-Developer", "Nabil Sahsada");
    res.setHeader("contact", "nabilsahsadacode@gmail.com");
    next();
});
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


// router
router.get("/", (req, res) => {
    res.json({
        status: "success",
        message: "Welcome to API",
    });
});

router.post("/auth", authController.authenticate);

// Api member
// Create
router.post("/member/create", authController.authorize, memberController.store);
// Read
router.get("/member/findAll", authController.authorize, (req, res) => {
    memberController.index().then((result) => {
        res.status(200).send(result)
    })
})
router.get("/member/findby", authController.authorize, (req, res) => {
    if (req.query.id) {
        var id = req.query.id
        memberController.showByid(id).then((result) => {
            res.status(200).send(result)
        })
    } else if (req.query.nama) {
        var nama = req.query.nama
        memberController.showAllbyname(nama).then((result) => {
            res.status(200).send(result)
        })
    }

})
// Update
router.put("/member/update", authController.authorize, memberController.update);

// Delete
router.delete("/member/delete", authController.authorize, (req, res) => {
    let id = req.query.id
    memberController.destroy(id).then((result) => {
        res.status(200).send(result)
    })
});

// Api admin
// Create
router.post("/admin/create", checker.validate('createAdmin'), checker.checkValidationResult, authController.authorize, (req, res) => {
    var post = {
        name: req.body.name,
        contact: req.body.contact,
        address: req.body.address,
        username: req.body.username,
        password: req.body.password,
    }
    adminController.store(post).then((result) => {
        res.status(200).send(result)
    })
});
// Read
router.get("/admin/findAll", authController.authorize, (req, res) => {
    adminController.index().then((result) => {
        res.status(200).send(result)
    })
})
router.get("/admin/findby", authController.authorize, (req, res) => {
    if (req.query.id) {
        var id = req.query.id
        adminController.showByid(id).then((result) => {
            res.status(200).send(result)
        })
    } else if (req.query.nama) {
        var nama = req.query.nama
        adminController.showAllbyname(nama).then((result) => {
            res.status(200).send(result)
        })
    }

})
// Update
router.put("/admin/update", checker.validate('updateAdmin'), checker.checkValidationResult, authController.authorize, (req, res) => {
    var post = {
        id: req.body.id,
        name: req.body.nama,
        contact: req.body.kontak,
        address: req.body.alamat,
        username: req.body.username,
        password: req.body.password,
    }
    adminController.update(post).then((result) => {
        res.status(200).send(result)
    })
});
// Delete
router.delete("/admin/delete", authController.authorize, (req, res) => {
    var id = req.body.id
    adminController.destroy(id).then((result) => {
        res.status(200).send(result)
    })
});


// Api buku
// Create
router.post("/buku/create", bukuController.store);
// Read
router.get("/buku/findAll", (req, res) => {
    bukuController.index().then((result) => {
        res.status(200).send(result)
    })
})
router.get("/buku/image/:image", bukuController.image);

router.get("/buku/findby", (req, res) => {
    if (req.query.id) {
        var id = req.query.id
        bukuController.showByid(id).then((result) => {
            res.status(200).send(result)
        })
    } else if (req.query.nama) {
        var nama = req.query.nama
        bukuController.showAllbyname(nama).then((result) => {
            res.status(200).send(result)
        })
    }

})
// Update
router.put("/buku/update", bukuController.update);

// Delete
router.delete("/buku/delete", (req, res) => {
    var id = req.query.id
    bukuController.destroy(id).then((result) => {
        res.status(200).send(result)
    })
});

// Api Borrow
// Create
router.post("/borrow/create", checker.validate('createBorrow'), checker.checkValidationResult, authController.authorize, borrowController.store);
// Read
router.get("/borrow/findAll", authController.authorize, (req, res) => {
    borrowController.index().then((result) => {
        res.status(200).send(result)
    })
})
router.get("/borrow/findby", authController.authorize, (req, res) => {
    if (req.query.id) {
        var id = req.query.id
        borrowController.showByid(id).then((result) => {
            res.status(200).send(result)
        })
    } else if (req.query.nama) {
        var nama = req.query.nama
        borrowController.showAllbyname(nama).then((result) => {
            res.status(200).send(result)
        })
    }

})
// Update
router.post("/borrow/update", checker.validate('updateBorrow'), checker.checkValidationResult, authController.authorize, borrowController.update);

// Delete
router.delete("/borrow/delete", authController.authorize, borrowController.destroy);

// Return book
router.get("/borrow/return", authController.authorize, borrowController.returnBook);

//Tugas Praktikum 
router.get("/borrow/filter", authController.authorize, borrowController.filter);


router.use(function (req, res) {
    res.status(404).json({
        status: 404,
        message: "Not Found",
    });
});


module.exports = router;
