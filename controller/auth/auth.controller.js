// Init Path
__path = process.cwd();
// End Init Path

// Init Libary
var md5 = require('md5');
var jwt = require('jsonwebtoken');

// init Model
const adminModel = require(`../../models/index`).admin

const authenticate = async (request, response) => {
    let dataLogin = {
        username: request.body.username,
        password: md5(request.body.password)
    }
    let dataAdmin = await adminModel.findOne({
        where: dataLogin
    })
    if (dataAdmin) {
        let payload = JSON.stringify(dataAdmin)
        /** define secret key as signature */
        let secret = `mokleters`
        /** generate token */
        let token = jwt.sign(payload, secret)
        /** define response */
        return response.json({
            success: true,
            logged: true,
            message: `Authentication Successed`,
            token: token,
            data: dataAdmin
        })
    }
    /** if data admin is not exists */
    return response.json({
        success: false,
        logged: false,
        message: `Authentication Failed. Invalid username or
    password`
    })
}

const authorize = (request, response, next) => {
    /** get "Authorization" value from request's header */
    let headers = request.headers.authorization
    /** when using Bearer Token for authorization,
    * we have to split `headers` to get token key.
    * valus of headers = `Bearers tokenKey`
    */let tokenKey = headers && headers.split(" ")[1]
    /** check nullable token */
    if (tokenKey == null) {
        return response.json({
            success: false,
            message: `Unauthorized User`
        })
    }
    /** define secret Key (equals with secret key in
    authentication function) */
    let secret = `mokleters`
    /** verify token using jwt */
    jwt.verify(tokenKey, secret, (error, user) => {
        /** check if there is error */
        if (error) {
            return response.json({
                success: false,
                message: `Invalid token`
            })
        }
    })
    /** if there is no problem, go on to controller */
    next()
}
module.exports = {
    authenticate,
    authorize
}