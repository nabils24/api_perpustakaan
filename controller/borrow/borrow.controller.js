// Init Path
__path = process.cwd();
// End Init Path


// Init Model
const borrowModel = require(`../../models/index`).borrow;
const detailsOfBorrowModel = require(`../../models/index`).details_of_borrow;

//Init Op
const Op = require(`sequelize`).Op

//init funct
async function store(request, response) {
    /** run function upload */

    let newData = {
        memberID: request.body.memberID,
        adminID: request.body.adminID,
        date_of_borrow: request.body.date_of_borrow,
        date_of_return: request.body.date_of_return,
        status: request.body.status
    }

    borrowModel.create(newData)
        .then(result => {
            /** get the latest id of book borrowing */
            let borrowID = result.id
            /** store details of book borrowing from request
            * (type: array object)
            */
            let detailsOfBorrow = request.body.details_of_borrow
            /** insert borrowID to each item of detailsOfBorrow 
            */

            for (let i = 0; i < detailsOfBorrow.length; i++) {
                detailsOfBorrow[i].borrowID = borrowID
            }
            /** insert all data of detailsOfBorrow */
            detailsOfBorrowModel.bulkCreate(detailsOfBorrow)
                .then(result => {
                    return response.json({
                        success: true,
                        message: `New Book Borrowed has been inserted`

                    })
                })
                .catch(error => {
                    return response.json({
                        success: false,
                        message: error.message
                    })
                })
        })
        .catch(error => {
            return response.json({
                success: false,
                message: error.message
            })
        })
}


async function index() {
    try {
        const data = await borrowModel.findAll(
            {
                include: [
                    `member`, `admin`,
                    {
                        model: detailsOfBorrowModel,
                        as: `details_of_borrow`,
                        include: ["book"]
                    }
                ]
            }
        );
        let res = {
            status: "success",
            message: "Data berhasil ditampilkan",
            data,
        };
        return res;
    } catch (error) {
        let res = {
            status: "error",
            message: error.message,
        };
        return res;
    }
}

async function showAllbyname(name) {
    try {
        const data = await borrowModel.findAll({
            where: {
                name: {
                    [Op.like]: `%${name}%`,
                },
            },
        });
        let res = {
            status: "success",
            message: "Data berhasil ditampilkan",
            data,
        };
        return res;

    } catch (error) {
        let res = {
            status: "error",
            message: error.message,
        };
        return res;
    }

}

async function showByid(id) {
    try {
        const data = await borrowModel.findOne({
            where: {
                id: id,
            },
        });
        let res = {
            status: "success",
            message: "Data berhasil ditampilkan",
            data,
        };
        return res;

    } catch (error) {
        let res = {
            status: "error",
            message: error.message,
        };
        return res;
    }

}

async function update(request, response) {
    try {
        let newData = {
            memberID: request.body.memberID,
            adminID: request.body.adminID,
            date_of_borrow: request.body.date_of_borrow,
            date_of_return: request.body.date_of_return,
            status: request.body.status
        }
        let borrowID = request.body.id

        borrowModel.update(newData, { where: { id: borrowID } })
            .then(async result => {
                /** if update's process success */

                await detailsOfBorrowModel.destroy(
                    { where: { borrowID: borrowID } }
                )

                let detailsOfBorrow = request.body.details_of_borrow

                for (let i = 0; i < detailsOfBorrow.length; i++) {
                    detailsOfBorrow[i].borrowID = borrowID
                }
                detailsOfBorrowModel.bulkCreate(detailsOfBorrow)
                    .then(result => {
                        return response.json({
                            success: true,
                            message: `Book Borrowed has been updated`

                        })
                    })
                    .catch(error => {
                        return response.json({
                            success: false,
                            message: error.message
                        })
                    })
            })

    } catch (error) {
        let res = {
            status: "error",
            message: error.message,
        };
        return res;
    }
}

async function destroy(request, response) {
    try {
        let borrowID = request.query.borrowID

        detailsOfBorrowModel.destroy(
            { where: { borrowID: borrowID } }
        )
            .then(result => {
                /** delete borrow's data using model */
                borrowModel.destroy({ where: { id: borrowID } })
                    .then(result => {
                        return response.json({
                            success: true,
                            message: `Borrowing Book's has deleted`
                        })
                    })
                    .catch(error => {
                        return response.json({
                            success: false,
                            message: error.message
                        })
                    })
            })

    } catch (error) {
        let res = {
            status: "error",
            message: error.message,
        };
        return res;
    }
}

async function returnBook(request, response) {
    try {
        let borrowID = request.query.id;
        let today = new Date()
        let currentDate = `${today.getFullYear()}-${today.getMonth()
            + 1}-${today.getDate()}`

        let newData = {
            date_of_return: currentDate,
            status: true
        }

        borrowModel.update(newData, { where: { id: borrowID } })
            .then(result => {
                return response.json({
                    success: true,
                    message: `Book Borrowed has been updated`

                })
            })
            .catch(error => {
                return response.json({
                    success: false,
                    message: error.message
                })
            })
    } catch (error) {
        return response.json({
            success: false,
            message: error.message
        })

    }
}

//Filter
async function filter(request, response) {
    try {
        let memberID = request.query.memberID;
        let status = request.query.status;
        let state = 0;
        if (status == "true") {
            state = 1
        }
        if (status == "false") {
            state = 0
        }
        if (memberID) {
            borrowModel.findAll({
                where: {
                    memberID: memberID
                },
                include: [
                    `member`, `admin`,
                    {
                        model: detailsOfBorrowModel,
                        as: `details_of_borrow`,
                        include: ["book"]
                    }
                ]
            })
                .then(result => {
                    return response.json({
                        success: true,
                        message: `Member With ID ${memberID}`,
                        data: result
                    })
                }
                )
                .catch(error => {
                    return response.json({
                        success: false,
                        message: error.message
                    })
                }
                )
        } else if (status) {
            borrowModel.findAll({
                where: {
                    status: state
                },
                include: [
                    `member`, `admin`,
                    {
                        model: detailsOfBorrowModel,
                        as: `details_of_borrow`,
                        include: ["book"]
                    }
                ]
            })
                .then(result => {
                    return response.json({
                        success: true,
                        message: `Member with status ${status}`,
                        data: result
                    })
                }
                )
                .catch(error => {
                    return response.json({
                        success: false,
                        message: error.message
                    })
                }
                )
        }


    } catch (error) {
        return response.json({
            success: false,
            message: error.message
        })

    }
}


module.exports = {
    store,
    index,
    showAllbyname,
    showByid,
    update,
    destroy,
    returnBook,
    filter

}